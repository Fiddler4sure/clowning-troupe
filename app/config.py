"""Flask configuration."""
from os import environ, path
from dotenv import load_dotenv

basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, '.env'))


class Config:
    """Base config."""
    SECRET_KEY = environ.get('SECRET_KEY')
    HOST = environ.get('HOST')
    USER = environ.get('USER')
    PASSWORD = environ.get('PASSWORD')
    DATABASE = environ.get('DATABASE')
    LOG_FILE =environ.get('LOG_FILE')